import {
    CHANGE_LOGIN_FORM,
    GET_USER,
    SET_LOADING,
    SET_LOGIN_FORM_ERRORS,
    INIT_URL, SIGNIN_SUCCESS,
} from "../constants/ActionTypes";

const INIT_STATE = {
    initUrl: "",
    username: "",
    password: "",
    isLoading: false,
    formErrors: {
        username: {
            error: false,
            message: ""
        },
        password: {
            error: false,
            message: ""
        }
    },
    authUser: null,
    access_token: JSON.parse(localStorage.getItem('access_token')),
    refresh_token: JSON.parse(localStorage.getItem('refresh_token')),
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case INIT_URL: {
            return {...state, initURL: action.payload};
        }
        case GET_USER: {
            return {
                ...state,
                authUser: action.payload
            };
        }
        case CHANGE_LOGIN_FORM: {
            return {
                ...state,
                [action.payload.name]: action.payload.value,
            }
        }
        case SET_LOGIN_FORM_ERRORS: {
            return {
                ...state,
                formErrors: {
                    ...state.formErrors,
                    ...action.payload,
                },
            }
        }
        case SET_LOADING: {
            return {
                ...state,
                isLoading: action.payload,
            }
        }
        case SIGNIN_SUCCESS: {
            return {
                ...state,
                access_token: action.payload.access_token,
                refresh_token: action.payload.refresh_token,
            }
        }
        default:
            return state;
    }
}
