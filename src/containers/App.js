import React, {Component, useEffect, useState} from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import {connect, useDispatch, useSelector} from 'react-redux';

import MainApp from '../app/index';
import SignIn from './Signin';
import {getUser, setInitUrl} from '../actions/Auth';
import axios from '../util/Api';
import asyncComponent from '../util/asyncComponent';
import CircularProgress from "../components/CircularProgress";

const RestrictedRoute = ({component: Component, authUser, token, ...rest}) => {
    const dispatch = useDispatch();
    const [auth, setAuth] = useState(false);
    const [isTokenValidated, setIsTokenValidated] = useState(false);
    const accessToken = useSelector(state => state.auth.access_token);
    const refreshToken = useSelector(state => state.auth.refresh_token);
    useEffect(() => {
        if (accessToken && refreshToken) {
            axios.post('api/token/verify/', {token: accessToken})
                .then((res) => {
                    if (res.status === 200) {
                        setAuth(true);
                        if (!authUser) {
                            axios.defaults.headers.common['Authorization'] = "Bearer " + accessToken;
                            dispatch(getUser());
                        }
                    }
                })
                .catch((err) => {
                    setAuth(false);
                    localStorage.clear();
                })
                .then(() => setIsTokenValidated(true));
        } else {
            setIsTokenValidated(true);
        }
    }, []);

    if (!isTokenValidated) return <CircularProgress/>;

    return (
        <Route
            {...rest}
            render={props => {
                return (
                    auth
                        ? <Component {...props} />
                        : <Redirect
                            to={{
                                pathname: '/signin',
                                state: {from: props.location}
                            }}
                        />
                )
            }}
        />)
};


class App extends Component {

    componentWillMount() {
        if (this.props.initURL === '') {
            this.props.setInitUrl(this.props.history.location.pathname);
        }
    }

    render() {
        const {match, location, access_token, refresh_token, authUser,} = this.props;
        if (location.pathname === '/') {
            if (access_token === null || refresh_token === null) {
                return (<Redirect to={'/signin'}/>);
            } else {
                return (<Redirect to={'/app/dashboard'}/>);
            }
        }

        return (
            <div className="app-main">
                <Switch>
                    <RestrictedRoute path={`/app`} authUser={authUser} token={access_token}
                                     component={MainApp}/>
                    <Route path='/signin' component={SignIn}/>
                    <Route
                        component={asyncComponent(() => import('../components/Error404'))}/>
                </Switch>
            </div>
        );
    }
}

const mapStateToProps = ({auth}) => {
    const {authUser, token, refresh_token, initURL} = auth;
    return {authUser, token, refresh_token, initURL};
};

export default connect(mapStateToProps, {getUser, setInitUrl})(App);