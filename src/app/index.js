import React from 'react';
import {Route, Switch, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import Header from '../components/Header/index';
import asyncComponent from '../util/asyncComponent';

class App extends React.Component {

    render() {
        return (
            <div className='app-container'>
                <div className="app-main-container">
                    <Header/>
                </div>
                <main className="app-main-content-wrapper">
                    <div className="app-main-content">
                        <Switch>
                            <Route path={`/app/dashboard`} component={asyncComponent(() => import('./routes/Dashboard'))}/>
                            <Route component={asyncComponent(() => import('../components/Error404'))}/>
                        </Switch>
                    </div>
                </main>
            </div>
        );
    }
}

export default withRouter(App);