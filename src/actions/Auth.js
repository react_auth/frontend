import axios from '../util/Api';
import {
    CHANGE_LOGIN_FORM,
    SET_LOGIN_FORM_ERRORS,
    SET_LOADING,
    SIGNIN_SUCCESS,
    SIGNIN_ERROR,
    INIT_URL, GET_USER,
} from "../constants/ActionTypes";


export const signin = (formObject, _history) => (dispatch) => {
    dispatch({type: SET_LOADING, payload: true});
    axios.post('api/token/', formObject)
        .then(res => {
            if (res.status === 200) {
                const access_token = JSON.stringify(res.data.access);
                const refresh_token = JSON.stringify(res.data.refresh);
                axios.defaults.headers.common['Authorization'] = "Bearer " + res.data.access;
                localStorage.setItem('access_token', access_token);
                localStorage.setItem('refresh_token', refresh_token);
                dispatch({
                    type: SIGNIN_SUCCESS, payload: {
                        access_token: res.data.access,
                        refresh_token: res.data.refresh,
                    }
                });
                dispatch({type: SET_LOADING, payload: false});
                _history.push('/app/dashboard');
            }
        })
        .catch(err => console.log(err));
};

export const changeLoginForm = (fieldObject) => (dispatch) => {
    return dispatch({type: CHANGE_LOGIN_FORM, payload: fieldObject});
};

export const setLoginFormErrors = (formErrors) => (dispatch) => {
    return dispatch({type: SET_LOGIN_FORM_ERRORS, payload: formErrors});
};

export const getUser = () => (dispatch) => {
    axios.post('api/accounts/me/')
        .then(res => {
            if(res.status === 200){
                dispatch({type: GET_USER, payload: res.data.data});
            }
        })
        .catch(err => console.log(err))
};

export const setInitUrl = (url) => {
    return {
        type: INIT_URL,
        payload: url
    };
};

